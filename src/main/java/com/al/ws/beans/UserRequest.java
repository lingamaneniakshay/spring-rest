package com.al.ws.beans;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Data
@Component
public class UserRequest {
    private String fName;
    private String lName;
    private String emailName;
    private String userName;
    private String userId;
}
