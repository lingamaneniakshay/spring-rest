package com.al.ws.beans;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Component
public class UserRequestModel {
    @NotNull(message = "FirstName cannot be null")
    @Size(min = 5)
    private String firstName;
    @NotNull(message = "LastName cannot be null")
    private String lastName;
    @NotNull
    @Email(message = "Please enter a valid email")
    private String emailName;
    @NotNull(message = "username cannot be null")
    private String userName;
}
