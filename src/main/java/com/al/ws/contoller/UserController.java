package com.al.ws.contoller;

import com.al.ws.beans.UserRequest;
import com.al.ws.beans.UserRequestModel;
import com.al.ws.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("users")
public class UserController {
    @Autowired
    UserRequest userRequest;
    @Autowired
    UserService userService;
    @Autowired
    Map<String, UserRequest> localMap;
    @GetMapping
    public String getUser(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "limit", defaultValue = "10") Integer limit) {
        return "default user was called and the userId is: " + page + " " + limit;
    }

    @GetMapping(path = "/{userId}")
    public ResponseEntity getUser(@PathVariable String userId) {
        return localMap.containsKey(userId) ? new ResponseEntity<>(localMap.get(userId), HttpStatus.OK) : new ResponseEntity("not present in localmap", HttpStatus.BAD_REQUEST);
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity createUser(@Valid @RequestBody UserRequestModel userRequestModel) {
       UserRequest userRequest = userService.createUser(userRequestModel);
        return new ResponseEntity<>(userRequest, HttpStatus.OK);
    }

    @PutMapping
    public String updateUser() {
        return "update user called";
    }

    @DeleteMapping
    public String deleteUser() {
        return "deleteUser called";
    }
}
