package com.al.ws.service;

import com.al.ws.beans.UserRequest;
import com.al.ws.beans.UserRequestModel;
import org.springframework.stereotype.Service;

public interface UserService {
    UserRequest createUser(UserRequestModel userRequestModel);
}
