package com.al.ws.service;

import com.al.ws.beans.UserRequest;
import com.al.ws.beans.UserRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
@Service
public class UserServiceImpl implements UserService{
    @Autowired
    UserRequest userRequest;
    Map<String,UserRequest> localMap;
    Utils utils;

    @Autowired
    public UserServiceImpl(Utils utils) {
        this.utils = utils;
    }

    @Override
    public UserRequest createUser(UserRequestModel userRequestModel) {
        userRequest.setUserName(userRequestModel.getUserName());
        userRequest.setFName(userRequestModel.getFirstName());
        userRequest.setLName(userRequestModel.getLastName());
        userRequest.setEmailName(userRequestModel.getEmailName());
        String uuid = utils.getUUID();
        userRequest.setUserId(uuid);
        if (localMap == null) localMap = new HashMap<>();
        localMap.put(uuid, userRequest);
        return userRequest;
    }
}
