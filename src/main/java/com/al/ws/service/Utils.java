package com.al.ws.service;

import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class Utils {
    public String getUUID(){
        return UUID.randomUUID().toString();
    }
}
